$(window).ready(function() {
    $(".header-info-video img").click(function() {
        var str = '<iframe src="https://player.vimeo.com/video/72202143" ' +
        'frameborder="0" webkitallowfullscreen ' +
        'mozallowfullscreen allowfullscreen></iframe>';
        $(this).parent().html(str).css("padding-top","0").css("padding-bottom","27.7%");
        $(window).resize();
    });
    $(window).resize(function() {
        $iframe = $("iframe");
        $iframe.css("height",$iframe.parent().css('padding-bottom'));
        $iframe.css("width",$iframe.parent().width() + 3);
    });
});
